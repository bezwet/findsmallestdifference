package test;

import java.util.stream.Stream;

import main.SmallestDifference;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SmallestDifferenceTest {

    @ParameterizedTest(name = "Array 1 content {0}, array 2 content {1}.  Smallest difference is {2}.")
    @MethodSource("findSmallestDifference")
    public void smallestDifferenceTest_test(int[] arr, int[] arr2, Integer expectedResult) {
        SmallestDifference smallestDifference = new SmallestDifference();

        Integer difference = smallestDifference.findSmallestDifference(arr, arr2);
        assertEquals(expectedResult, difference);
    }

    public static Stream<Arguments> findSmallestDifference() {
        return Stream.of(
                Arguments.of(new int[]{10, 5, 2, 2},new int[]{44, -5, -12, -1}, 3),
                Arguments.of(new int[]{100, 100}, new int[]{0, -1, 0, 200}, 100),
                Arguments.of(new int[]{100, 100}, new int[]{0, -1, 0, 100}, 0),
                Arguments.of(null, new int[]{5, 10, 15, 20}, -1),
                Arguments.of(new int[]{5, 10, 15, 20}, null, -1),
                Arguments.of(null, null, -1),
                Arguments.of(new int[]{}, new int[]{10, 5, 2, 2}, -1));
    }
}

package main;

import java.util.Arrays;

public class SmallestDifference {
    public int findSmallestDifference(int[] array1, int[] array2) {
        if (array1 == null || array2 == null || array1.length == 0 || array2.length == 0) {
            return -1;
        }
        Arrays.sort(array1);
        Arrays.sort(array2);

        int difference = Integer.MAX_VALUE;

        int pointerArray1 = 0;
        int pointerArray2 = 0;
        while (pointerArray1 < array1.length && pointerArray2 < array2.length) {
            int tempDifference = Math.abs(array1[pointerArray1] - array2[pointerArray2]);
            if (difference > tempDifference) {
                difference = tempDifference;
            }
            if (array1[pointerArray1] > array2[pointerArray2]) {
                pointerArray2++;
            } else {
                pointerArray1++;
            }

        }
        return difference;

    }

}
